package com.paic.arch.interviews;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author 24652  实现接口定义
 *
 * @date 2018/3/8
 */
public class TimeConverterImpl implements  TimeConverter {

    @Override
    public String convertTime(String aTime) {

        if(StringUtils.isBlank(aTime)){
            throw new RuntimeException("aTime 不能为空字符串");
        }
        TimeParse timeParse = TimeParseUtils.instance.parseTime(aTime);
        return TimeParseUtils.instance.
                printTime(timeParse.getHour(),timeParse.getMinute(),timeParse.getSencond());


    }

}
