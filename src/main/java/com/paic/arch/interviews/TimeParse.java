package com.paic.arch.interviews;

/**
 * Created by 24652 on 2018/3/8.
 */
public class TimeParse {

    private Integer hour;

    private Integer minute;

    private Integer sencond;

    public TimeParse(Integer hour, Integer minute, Integer sencond) {
        this.hour = hour;
        this.minute = minute;
        this.sencond = sencond;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSencond() {
        return sencond;
    }

    public void setSencond(Integer sencond) {
        this.sencond = sencond;
    }

}
