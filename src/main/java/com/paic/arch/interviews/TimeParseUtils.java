package com.paic.arch.interviews;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author 24652
 * @date 2018/3/8
 */
public enum TimeParseUtils {

    instance;

    private static  final Integer CELL_SIZE = 4 ;
    private static  final Integer NUM = 5 ;

    private static  final Integer MINUTE_CELL_SIZE = 11 ;
    private static  final String FILE_ENTER = System.getProperty("line.separator");
    private static  final String RED = "R";
    private static  final String YELLOW = "Y";
    private static  final String ZERO = "O";

    private static  final String TIME_MAX = "24:00:00";


    /**
     * 解析时间，获取 小时，分钟，秒数
     * @param aTime
     * @return
     */
    public TimeParse parseTime(String aTime){
        SimpleDateFormat dateFormat =new SimpleDateFormat("HH:mm:ss");
        Calendar calendar = Calendar.getInstance();

        try{
            Date date = dateFormat.parse(aTime);
            calendar.setTime(date);
        }catch (Exception e){
            throw new RuntimeException("aTime数据格式不正确");
        }
        if(TIME_MAX.equals(aTime)){
           return new TimeParse(24,0,0);
        }
        int hour =calendar.get(Calendar.HOUR_OF_DAY);
        int minute  =calendar.get(Calendar.MINUTE);
        int second =calendar.get(Calendar.SECOND);
        return new TimeParse(hour,minute,second);
    }

    /**
     * 拼接字符串，方便打印。
     * @param hour
     * @param minute
     * @param second
     * @return
     */
    public String printTime(Integer hour,Integer minute,Integer second){
        StringBuilder sb =new StringBuilder();
        printTop(second,sb);
        printHour(hour,sb);
        printMinute(minute,sb);
        String result = sb.toString();

        return result;
    }

    /**
     * 打印秒灯
     * @param second
     * @param sb
     */
    void printTop(Integer second,StringBuilder sb){
        sb.append(second% 2 == 0 ? YELLOW : ZERO);
        sb.append(FILE_ENTER);
    }

    private void printHour(Integer hour,StringBuilder sb){
        Integer first = hour / NUM;
        Integer second = hour -(first*NUM);
        //打印第一行的数据
        printRed(first,sb);
        //打印第二行的数据
        printRed(second,sb);
    }

    void print( Integer first,StringBuilder sb,String color){
        if(first==0){
            for(int index =1; index<=CELL_SIZE; index++){
                sb.append(ZERO);
            }
            enter(sb);
            return;
        }
        for(int index =1; index<=CELL_SIZE; index++){
            if(index<=first){
                sb.append(color);
            }else{
                sb.append(ZERO);
            }
        }
        enter(sb);
    }

    void printRed( Integer first,StringBuilder sb){
        print(first,sb,RED);
    }

    void printYellow( Integer first,StringBuilder sb){
        print(first,sb,YELLOW);
    }


    private void printMinute(Integer minute,StringBuilder sb){
        Integer first = minute / NUM;
        Integer second = minute -(first*NUM);

        //打印第三行的数据
        if(first==0){
            for(int index =1; index<=MINUTE_CELL_SIZE; index++){
                sb.append(ZERO);
            }
            enter(sb);
            printYellow(second,sb);
            removeEnter(sb);
            return;
        }
        for(int index =1; index<=MINUTE_CELL_SIZE; index++){
            if(index <= first){
                if(index ==3 || index ==6 ||index==9){
                    sb.append(RED);
                }else{
                    sb.append(YELLOW);
                }
            }else{
                sb.append(ZERO);
            }
        }
        enter(sb);

        //打印第四行数据
        printYellow(second,sb);
        removeEnter(sb);

    }

    /**
     * 添加换行字符串
     * @param sb
     */
    private void enter(StringBuilder sb){
        sb.append(FILE_ENTER);
    }

    /**
     * 删除换行字符串
     * @param sb
     */
    private void removeEnter(StringBuilder sb){
        sb.delete(sb.lastIndexOf(FILE_ENTER),sb.length());
    }

}
